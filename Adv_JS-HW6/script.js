"use strict";

const root = document.querySelector("#root");
const URL = "https://api.ipify.org/?format=json";
const urlService = "http://ip-api.com/";

class Button {
  constructor(root) {
    this.root = root;
  }

  renderButton() {
    const btn = document.createElement("button");
    btn.textContent = "Знайти по IP";

    this.root.append(btn);

    btn.addEventListener("click", (event) => {
      getIP(URL)
        .then((dataIP) => getPhysicalAddress(urlService, dataIP))
        .then((dataAdress) => inform.renderAdress(dataAdress))
        .catch((err) => {
          console.log(err);
        });
    });
  }
}
new Button(root).renderButton();

class Inform {
  constructor(root) {
    this.root = root;
  }
  // континент, країна, регіон, місто, район
  renderAdress({ timezone, country, regionName, city, region }) {
    const infoAdress = document.createElement("p");
    infoAdress.textContent = `Континент: ${timezone}. Країна: ${country}. Регіон: ${regionName}. Місто: ${city}. Район: ${region}.`;

    root.append(infoAdress);
  }
}
const inform = new Inform(root);

async function getIP(url) {
  const response = await fetch(url);
  const dataIP = await response.json();

  console.log(dataIP);
  return dataIP;
}

async function getPhysicalAddress(urlService, { ip }) {
  const response = await fetch(`${urlService}json/${ip}`);
  const dataAdress = await response.json();

  console.log(dataAdress);
  return dataAdress;
}
