"use strict";

// const urlUsers = "https://ajax.test-danit.com/api/json/users";
// const urlPosts = "https://ajax.test-danit.com/api/json/posts";

// class Request {
//   get(url) {
//     return fetch(url).then((response) => response.json());
//   }
// }

// const users = new Request().get(urlUsers);
// console.log(users);
// const posts = new Request().get(urlPosts);
// console.log(posts);

const URL = "https://ajax.test-danit.com/api/json/";
const root = document.querySelector("#root");

class RequestEntity {
  get(url, entity) {
    return fetch(url + entity).then((response) => response.json());
  }
}

new RequestEntity()
  .get(URL, "users")
  .then((dataUsers) => {
    // console.log(dataUsers);
    new RequestEntity()
      .get(URL, "posts")
      .then((dataPosts) => {
        // console.log(dataPosts);
        dataPosts.forEach((post) => {
          dataUsers.forEach((user) => {
            if (user.id === post.userId) {
              post.name = user.name;
              post.email = user.email;
            }
          });
        });
        // console.log(dataPosts);
        dataPosts.forEach((post) => {
          new Card().render(post);
        });
      })
      .catch((err) => console.log(err));
  })
  .catch((err) => console.log(err));

class Card {
  render({ id: postId, name, email, title, body: postText }) {
    const cardWrap = document.createElement("div");
    cardWrap.style.cssText =
      "padding: 10px; margin: 5px; border: 3px solid pink";

    const pUser = document.createElement("p");
    pUser.textContent = `${name}, ${email} (${postId})`;
    const pTitle = document.createElement("p");
    pTitle.textContent = title;
    const pText = document.createElement("p");
    pText.textContent = postText;

    const btnDel = document.createElement("button");
    btnDel.textContent = "Delete post";

    cardWrap.append(pUser, pTitle, pText, btnDel);
    root.append(cardWrap);

    btnDel.addEventListener("click", () => {
      new deletePost().del(postId).then((data) => {
        // console.log(data);
        if (data.ok === true) {
          cardWrap.remove();
        }
      });
    });
  }
}

class deletePost {
  del(postId) {
    return fetch(URL + "posts/" + postId, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        return response;
      })
      .catch((err) => console.log(err));
  }
}
