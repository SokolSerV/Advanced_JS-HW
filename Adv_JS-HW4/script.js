"use strict";

const url = "https://ajax.test-danit.com/api/swapi/films";
const root = document.getElementById("root");

class RequestFilmsList {
  constructor(url) {
    this.url = url;
  }
  getFilms() {
    return fetch(this.url).then((response) => {
      return response.json();
    });
  }
}

class StarWarsRender {
  constructor(root) {
    this.root = root;
  }
  renderFilmsList(data) {
    const filmsList = document.createElement("ol");
    // console.log(data);

    const filmsListItems = data.map(
      ({ episodeId, name, openingCrawl, characters }) => {
        const item = document.createElement("li");

        const pId = document.createElement("p");
        pId.textContent = `Номер епізоду: ${episodeId}`;
        const pName = document.createElement("p");
        pName.textContent = `Назву фільму: ${name}`;
        const pShort = document.createElement("p");
        pShort.textContent = `Короткий зміст: ${openingCrawl}`;

        item.append(pId, pName, pShort);

        // console.log(characters);
        const promiseObjectsPersons = characters.map((urlPerson) => {
          return fetch(urlPerson).then((response) => {
            return response.json();
          });
        });
        // console.log(promiseObjectsPersons);

        Promise.all(promiseObjectsPersons)
          .then((arrayObjectsPersons) => {
            // console.log(arrayObjectsPersons);
            const personsFilm = document.createElement("p");
            const arrayNamesPersons = arrayObjectsPersons.map(({ name }) => {
              return name;
            });
            const stringNamesPersons = arrayNamesPersons.join(", ");
            personsFilm.textContent = `Персонажі: ${stringNamesPersons}`;
            // console.log(personsFilm);
            item.append(personsFilm);
          })
          .catch((err) => {
            console.log(err);
          });
        return item;
      }
    );

    filmsList.append(...filmsListItems);
    this.root.append(filmsList);
  }
}

const requestFilmsList = new RequestFilmsList(url);
const starWarsRender = new StarWarsRender(root);

requestFilmsList
  .getFilms()
  .then((data) => {
    starWarsRender.renderFilmsList(data);
  })
  .catch((err) => {
    console.log(err);
  });
