"use strict";

const books = [
  { author: "Люсі Фолі", name: "Список запрошених", price: 70 },
  { author: "Сюзанна Кларк", name: "Джонатан Стрейндж і м-р Норрелл" },
  { name: "Дизайн. Книга для недизайнерів", price: 70 },
  { author: "Алан Мур", name: "Неономікон", price: 70 },
  { author: "Террі Пратчетт", name: "Рухомі картинки", price: 40 },
  { author: "Анґус Гайленд", name: "Коти в мистецтві" },
];

const root = document.getElementById("root");
const elemUl = document.createElement("ul");
root.append(elemUl);

for (let i = 0; i < books.length; i++) {
  try {
    if (!("author" in books[i])) {
      throw new Error(`Missing property "author" in book ${i + 1}`);
    }
    if (!("name" in books[i])) {
      throw new Error(`Missing property "name" in book ${i + 1}`);
    }
    if (!("price" in books[i])) {
      throw new Error(`Missing property "price" in book ${i + 1}`);
    }

    const elemLi = document.createElement("li");

    const autorSpan = document.createElement("span");
    autorSpan.textContent = `author: ${books[i].author}, `;

    const nameSpan = document.createElement("span");
    nameSpan.textContent = `name: ${books[i].name}, `;

    const priceSpan = document.createElement("span");
    priceSpan.textContent = `price: ${books[i].price}`;

    elemLi.append(autorSpan, nameSpan, priceSpan);
    elemUl.append(elemLi);
  } catch (error) {
    console.error(error);
  }
}
