"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

// const employee = new Employee("Employee", 18, 1000);
// console.log(employee);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary * 3;
  }

  set lang(value) {
    this._lang = value;
  }
  get lang() {
    return this._lang;
  }
}

const programmer1 = new Programmer("Programmer-1", 18, 1000, "uk/eng");
console.log(programmer1);
console.log(programmer1.salary);

const programmer2 = new Programmer("Programmer-2", 28, 2000, "uk/eng/apa");
console.log(programmer2);
console.log(programmer2.salary);
